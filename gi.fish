#!/usr/bin/env/fish

function gi
  curl -L -s https://www.gitignore.io/api/$argv;
end